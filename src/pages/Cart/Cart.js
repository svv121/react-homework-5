import React, { useState } from 'react';
import CartItem from "../../components/CartItem/CartItem";
import styles from "./Cart.module.scss";
import PropTypes from "prop-types";
import Modal from "../../components/Modal/Modal";
import {useDispatch, useSelector} from "react-redux";
import openModalAC from '../../store/modal/actionCreatorsModal'
import BuyerForm from "../../components/BuyerForm/BuyerForm";

const Cart = (props) => {
    const [cartArr, setCartArr] = useState(JSON.parse(localStorage.getItem('cartArr')) || []);
    const [currentProduct, setCurrentProduct] = useState(null)

    const openCartModal = useSelector(state => state.modal.isOpenModal);
    const dispatch = useDispatch();

    const deleteFromCart = (id) => {
        const newCartArr = (JSON.parse(localStorage.getItem('cartArr')) || []).filter(el => el !== id);
        setCartArr(newCartArr);
        localStorage.setItem('cartArr', JSON.stringify(newCartArr));
        const productInCartCount = {};
        newCartArr.forEach(item => {productInCartCount[item] = (productInCartCount[item] || 0) + 1});
        localStorage.setItem('cartObj', JSON.stringify(productInCartCount));
    }

    const {incrementCartCount} = props;
    const products = useSelector(state => state.productsAll.products);
    const productsInCart = products.filter(el => cartArr.includes(el.id))

    return (
        <>
            <h1 className={styles.CartTitle}>Your Cart</h1>
            <BuyerForm cartArr={cartArr} productsInCart={productsInCart} setCartArr={setCartArr} incrementCartCount={incrementCartCount}/>
            <div className={styles.Cart}>
                {productsInCart.map(product => (
                    <CartItem
                        setCurrentProduct={setCurrentProduct}
                        productInfo={product}
                        key={product.id}
                        color={product.color}
                        url={product.url}
                        name={product.name}
                        id={parseInt(product.id)}
                        price={parseFloat(product.price)}
                        article={parseInt(product.article)}
                        currency="$"
                        openCartModal={openCartModal}
                    />
                ))}
                {
                    openCartModal &&
                    <Modal
                        header="Deleting this product from your cart"
                        closeButton = {true}
                        text={`You are going to delete: ${currentProduct.name} from your cart`}
                        LeftBtnTxt="Delete"
                        RightBtnTxt="Cancel"
                        bgColor="#F07465"
                        closeModal={() => dispatch(openModalAC(false))}
                        actions={() => {
                            incrementCartCount(-(JSON.parse(localStorage.getItem('cartArr'))).filter(el => el === currentProduct.id).length);
                            deleteFromCart(currentProduct.id);
                            dispatch(openModalAC(false));
                        }}
                    />
                }
            </div>
        </>
    );
}

Cart.propTypes = {
    products: PropTypes.array,
    incrementCartCount: PropTypes.func,
};

Cart.defaultProps = {
    products: [],
    incrementCartCount: () => {},
};

export default Cart;