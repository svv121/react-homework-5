import React, { useState } from 'react';
import Product from "../../components/Product/Product";
import styles from "./ProductsAll.module.scss";
import PropTypes from "prop-types";
import Modal from "../../components/Modal/Modal";
import {useDispatch, useSelector} from "react-redux";
import openModalAC from '../../store/modal/actionCreatorsModal'

const ProductsAll = (props) => {
  const [/*cartArr*/, setCartArr] = useState(JSON.parse(localStorage.getItem('cartArr')) || []);
  const [favoriteArr, setFavoriteArr] = useState(JSON.parse(localStorage.getItem('favoriteArr')) || []);
  const [currentProduct, setCurrentProduct] = useState(null)

  const openCartModal = useSelector(state => state.modal.isOpenModal);
  const dispatch = useDispatch();

  const addToCart = (id) => {
    const newCartArr = JSON.parse(localStorage.getItem('cartArr')) || [];
    newCartArr.push(id);
    setCartArr(newCartArr);
    localStorage.setItem('cartArr', JSON.stringify(newCartArr));
    const productInCartCount = {};
    newCartArr.forEach(item => {productInCartCount[item] = (productInCartCount[item] || 0) + 1});
    localStorage.setItem('cartObj', JSON.stringify(productInCartCount));
  }

  const changeFavoriteArr = (id) => {
    const index = favoriteArr.indexOf(id);
    const newFavoriteArr = [...favoriteArr];
    if(index !== -1) {
      newFavoriteArr.splice(index,1);
    } else {
      newFavoriteArr.push(id);
    }
    setFavoriteArr(newFavoriteArr);
    localStorage.setItem('favoriteArr', JSON.stringify(newFavoriteArr));
  }

    const {incrementCartCount, incrementFavoriteCount} = props;
    const products = useSelector(state => state.productsAll.products);

    return (
      <>
        <h1 className={styles.ProductsAllTitle}>Latest arrivals in KitGoods</h1>
        <div className={styles.ProductsAll}>
          {products.map(product => (
            <Product
                incrementFavoriteCount={incrementFavoriteCount}
                setCurrentProduct={setCurrentProduct}
                productInfo={product}
                key={product.id}
                color={product.color}
                url={product.url}
                name={product.name}
                id={parseInt(product.id)}
                price={parseFloat(product.price)}
                article={parseInt(product.article)}
                currency="$"
                openCartModal={openCartModal}
                changeFavoriteArr={changeFavoriteArr}
                isFavorite={favoriteArr.indexOf(product.id) !== -1}
            />
          ))}
          {
              openCartModal &&
              <Modal
                  header="Adding this product to your cart"
                  closeButton = {true}
                  text={`Once you add this product: ${currentProduct.name}, you can continue your purchases or you can checkout`}
                  LeftBtnTxt="Add"
                  RightBtnTxt="Cancel"
                  bgColor="#64B743"
                  closeModal={() => dispatch(openModalAC(false))}
                  actions={() => {
                    incrementCartCount(1);
                    addToCart(currentProduct.id);
                    dispatch(openModalAC(false));
                  }}
              />
          }
        </div>
      </>
    );
}

ProductsAll.propTypes = {
  incrementCartCount: PropTypes.func,
  incrementFavoriteCount: PropTypes.func
};

ProductsAll.defaultProps = {
  incrementCartCount: () => {},
  incrementFavoriteCount:  () => {}
};

export default ProductsAll;