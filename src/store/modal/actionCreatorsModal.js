import { IS_OPEN_MODAL } from './actionsModal'

const openModalAC = (payload) => dispatch => dispatch({ type: IS_OPEN_MODAL, payload })
export default openModalAC