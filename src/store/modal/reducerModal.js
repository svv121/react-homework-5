import { IS_OPEN_MODAL } from './actionsModal'

const initialState = {isOpenModal: false};

const reducerModal = (state = initialState, action) => {
    switch (action.type) {
        case IS_OPEN_MODAL: {
            return {...state, isOpenModal: action.payload}
        }
        default: {
            return state;
        }
    }
};

export default reducerModal