import { GET_PRODUCTS, CLEAR_CART_IN_LS } from './actionsProducts'

const initialState = {products: []}
const reducerProducts = (state = initialState, action) => {
    switch (action.type) {
        case GET_PRODUCTS: {
            return {...state, products: [...action.payload]}
        }
        case CLEAR_CART_IN_LS: {
            return {...state}
        }
        default: {
            return state
        }
    }
};

export default reducerProducts