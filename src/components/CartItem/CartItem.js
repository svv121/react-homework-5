import React from 'react';
import styles from "./CartItem.module.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import {useDispatch} from "react-redux";
import openModalAC from '../../store/modal/actionCreatorsModal'

const CartItem = (props) => {
    const {
        productInfo,
        currency,
        setCurrentProduct,
    } = props;
    const {id, name, price, url, article, color} = productInfo

    const dispatch = useDispatch();

    return (
        <div className={styles.case}>
            <div className={styles.deleteFromCart}>
                <Button
                    handleClick={() => {
                        setCurrentProduct(productInfo);
                        dispatch(openModalAC(true))}}
                    backgroundColor="#F07465"
                    text="&#9587;"
                    id={id}
                />
            </div>
            <div className={styles.productImgWrapper}>
                <img src={url} alt={name} className={styles.productImg} />
            </div>
            <h2 className={styles.name}>{name}</h2>
            <div className={styles.priceWrapper}>
                <h4 className={styles.price}>Price: {currency}{price}</h4>
                <div className={styles.countInfo}>
                    <p>Quantity: {+JSON.parse(localStorage.getItem('cartArr')).filter(el => el === id).length}</p>
                </div>
            </div>
            <p>Article: {article}</p>
            <p>Color: {color}</p>
        </div>
    );
}

CartItem.propTypes = {
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    name: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    url: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    currency: PropTypes.string.isRequired,
    article: PropTypes.number.isRequired,
    color: PropTypes.string.isRequired,
    incrementCartCount: PropTypes.func,
};

CartItem.defaultProps = {
    incrementCartCount: () => {},
};

export default CartItem;