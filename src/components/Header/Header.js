import React from 'react';
import { NavLink } from 'react-router-dom';
import styles from './Header.module.scss';
import PropTypes from "prop-types";
import { ReactComponent as StarEmpty } from "../../assets/svg/star-empty.svg";
import { ReactComponent as CartIcon } from "../../assets/svg/cart-icon.svg";

const Header = (props) => {
        const { favoriteCount, cartCount } = props;
        return (
            <header className={styles.header}>
                <div className={styles.logo}>
                    <NavLink className={styles.logoTextLink} to="/">KitGoods
                    </NavLink>
                </div>
                <div className={styles.countersWrapper}>
                 <div className={styles.countContainer}>
                    <NavLink className={({ isActive }) =>
                        isActive ? styles.linkText + ' ' + styles.active : styles.linkText}
                             to="/favorites">Favorites<StarEmpty className={styles.favoriteSvg} />
                    </NavLink>
                     <p>{favoriteCount}</p>
                 </div>
                    <div className={styles.countContainer}>
                     <NavLink className={({ isActive }) =>
                         isActive ? styles.linkText + ' ' + styles.active : styles.linkText}
                              to="/cart">Cart<CartIcon className={styles.cartSvg} />
                     </NavLink>
                     <p>{cartCount}</p>
                    </div>
                </div>
            </header>
        )
}

Header.propTypes = {
    favoriteCount: PropTypes.number,
    cartCount: PropTypes.number,
};

Header.defaultProps = {
    favoriteCount: 0,
    cartCount: 0,
};

export default Header;