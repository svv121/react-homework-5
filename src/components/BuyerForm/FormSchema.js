import * as yup from "yup";

const nameValidate = /^[\w'\-,.][^0-9_!¡?÷¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]{2,}$/
const phoneValidate = /^([+]?[\s0-9]+)?(\d{3}|[(]?[0-9]+[)])?(-?\s?[0-9])+$/

const FormSchema = yup.object().shape({
    firstName: yup
        .string()
        .min(2, 'Min 2 characters')
        .max(24, 'Max 24 characters')
        .matches(nameValidate, 'Invalid first name format')
        .required('This field is required'),
    lastName: yup
        .string()
        .min(2, 'Min 2 characters')
        .max(24, 'Max 24 characters')
        .matches(nameValidate, 'Invalid last name format')
        .required('This field is required'),
    email: yup
        .string()
        .email('Invalid email format')
        .required('This field is required'),
    age: yup
        .number()
        .min(16, "Min age is 16 years")
        .max(120, 'Max age is 120 years'),
    phoneNumber: yup
        .string()
        .matches(phoneValidate, 'Invalid phone number format')
        .min(10, 'Min 10 characters')
        .max(24, 'Max 24 characters')
        .required('This field is required'),
    address: yup
        .string()
        .min(8, 'Min 8 characters')
        .max(48, 'Max 200 characters')
        .required('This field is required'),
});

export default FormSchema;